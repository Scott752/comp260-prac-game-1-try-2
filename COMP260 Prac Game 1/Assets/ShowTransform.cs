﻿using UnityEngine;
using System.Collections;

public class ShowTransform : MonoBehaviour {

    // Use this for initialization

    public Vector2 velocity;

    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //scale the velocity by frame duration
        Vector2 move = velocity * Time.deltaTime;

        // move the object
        transform.Translate(move);


    }
}
